import { FormHelperText } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Link from '@material-ui/core/Link'
import TextField from '@material-ui/core/TextField'
import { action, decorate, observable, computed } from 'mobx'
import { inject, observer } from 'mobx-react'
import React from 'react'
import LoginWrapper from '../../../components/LoginWrapper/LoginWrapper'
import './RegisterScreen.scss'

class RegisterScreen extends React.Component {
  email = ''
  dsgvo = false
  voranmeldung = null
  errorMessage
  buttonTriggered = false

  setEmail = val => {
    this.email = val
  }

  setDsgvo = val => {
    if (val === true) {
      this.dsgvo = true
      this.setVoranmeldung(true)
    } else {
      this.dsgvo = false
      this.setVoranmeldung(null)
    }
  }

  submit = async () => {
    if(this.email && this.dsgvo) {
      let res = await this.props.store.visitorStore.register(this.email, this.voranmeldung)
      if(res === true)
        this.props.history.push('/')
      else
        this.setErrorMessage(res)
    }
    this.setButtonTriggered(true)
  }

  setVoranmeldung = val => {
    this.voranmeldung = val
  }

  setErrorMessage = val => {
    this.errorMessage = val
  }

  setButtonTriggered = val => {
    this.buttonTriggered =val
  }

  get validEmail() {
    return this.email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)   
  }

  render() {
    return (
      <LoginWrapper onSubmit={this.submit}>
        <img
          src="logo.svg"
          alt="HTL St. Pölten"
          className="block mx-auto w-4/5"
        />
        <div className="my-8">
          <TextField
            required
            label="E-Mail"
            type="email"
            autoComplete="email"
            fullWidth
            margin="none"
            value={this.email}
            onChange={e => this.setEmail(e.target.value)}
          />
        </div>
        <FormControlLabel
          label={<span style={{ fontSize: '0.7rem' }}>Ich bin damit einverstanden, dass meine Email-Adresse gespeichert wird *</span>}
          control={
            <Checkbox
              style={{ padding: '0.3rem 0.5rem' }}
              checked={this.dsgvo}
              onChange={e => this.setDsgvo(e.target.checked)}
            />
          }
        />
        <div className="mb-2">
          <FormControlLabel
            control={
              <Checkbox
                style={{ padding: '0.3rem 0.5rem' }}
                disabled={this.voranmeldung === null}
                checked={this.voranmeldung === true}
                onChange={e => this.setVoranmeldung(e.target.checked)}
              />
            }
            label={<span style={{ fontSize: '0.7rem' }}>Ich bin damit einverstanden, einen Link zur Voranmeldung per Mail zu erhalten</span>}
          />
        </div>
        {this.buttonTriggered && this.email === '' && <FormHelperText error>Email ist erfolderlich</FormHelperText>}
        {this.buttonTriggered && this.email !== '' && !this.validEmail && <FormHelperText error>Email ist ungültig</FormHelperText>}
        {this.buttonTriggered && !this.dsgvo && <FormHelperText error>Erlaubnis zum Abspeicherung der Email ist erfolderlich</FormHelperText>}
        {this.errorMessage && <FormHelperText error>{this.errorMessage}</FormHelperText>}
        <div className="flex justify-center mt-4">
          <Button
            variant="contained"
            color="primary"
            margin="none"
            onClick={this.submit}
          >
                Start
          </Button>
        </div>
        <div className="text-center leading-tight mt-4">
          <Link style={{ cursor: 'pointer' }} onClick={() => {
            this.props.history.push('/login')
          }}>
        Sie haben sich bereits angemeldet?
          </Link>
        </div>
      </LoginWrapper>
    )
  }
}

export default inject('store')(
  observer(
    decorate(RegisterScreen, {
      email: observable,
      dsgvo: observable,
      voranmeldung: observable,
      errorMessage: observable,
      buttonTriggered: observable,
      setEmail: action,
      setDsgvo: action,
      setVoranmeldung: action,
      setErrorMessage: action,
      setButtonTriggered: action,
      validEmail: computed
    })
  )
)
