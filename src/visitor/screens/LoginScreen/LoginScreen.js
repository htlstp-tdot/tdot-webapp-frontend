import { FormHelperText } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link'
import TextField from '@material-ui/core/TextField'
import { action, decorate, observable ,computed } from 'mobx'
import { inject, observer } from 'mobx-react'
import React from 'react'
import LoginWrapper from '../../../components/LoginWrapper/LoginWrapper'
import './LoginScreen.scss'

class LoginScreen extends React.Component {
  email = ''
  errorMessage
  buttonTriggered = false

  setEmail = val => {
    this.email = val
  }

  submit = async () => {
    if(this.email) {
      let res = await this.props.store.visitorStore.login(this.email)
      if(res === true)
        this.props.history.push('/')
      else
        this.setErrorMessage(res)
    }
    this.setButtonTriggered(true)
  }

  setErrorMessage = val => {
    this.errorMessage = val
  }

  setButtonTriggered = val => {
    this.buttonTriggered =val
  }

  get validEmail() {
    return this.email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)   
  }

  render() {
    return (
      <LoginWrapper onSubmit={this.submit}>
        <img
          src="logo.svg"
          alt="HTL St. Pölten"
          className="block mx-auto w-4/5"
        />
        <div className="my-8">
          <TextField
            required
            label="E-Mail"
            type="email"
            autoComplete="email"
            fullWidth
            margin="none"
            value={this.email}
            onChange={e => this.setEmail(e.target.value)}
          />
        </div>
       
        {this.buttonTriggered && this.email === '' && <FormHelperText error>Email ist erfolderlich</FormHelperText>}
        {this.buttonTriggered && this.email !== '' && !this.validEmail && <FormHelperText error>Email ist ungültig</FormHelperText>}        
        {this.errorMessage && <FormHelperText error>{this.errorMessage}</FormHelperText>}
        <div className="flex justify-center mt-4">
          <Button
            variant="contained"
            color="primary"
            margin="none"
            onClick={this.submit}
          >Login
          </Button>
        </div>
        <div className="text-center leading-tight mt-4">
          <Link style={{ cursor: 'pointer' }} onClick={() => {
            this.props.history.push('/register')
          }}>
        Sie haben sich noch nicht registriert?
          </Link>
        </div>
      </LoginWrapper>
    )
  }
}

export default inject('store')(
  observer(
    decorate(LoginScreen, {
      email: observable,
      errorMessage: observable,
      buttonTriggered: observable,
      setEmail: action,
      setErrorMessage: action,
      setButtonTriggered: action,
      validEmail: computed
    })
  )
)
