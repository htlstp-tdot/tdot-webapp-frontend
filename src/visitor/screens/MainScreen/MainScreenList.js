import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListSubheader from '@material-ui/core/ListSubheader'
import { action, decorate, observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import React from 'react'
import 'react-circular-progressbar/dist/styles.css'
import './MainScreen.scss'


class MainScreenList extends React.Component {
    open = false

    handleClick = () => {
      this.open= !this.open
    }

    render() {
      let { visitorStore: store } = this.props.store

      return (
        <List
          subheader={
            <ListSubheader disableSticky>
            Räume
            </ListSubheader>
          }>
          {store.booths.sort((a, b) => String(a.roomnumber).localeCompare(b.roomnumber)).map((booth, index) => {
            return <ListItem
              ref={
                index > 0 &&
                store.booths.sort((a, b) => String(a.roomnumber).localeCompare(b.roomnumber))[index-1].roomnumber === booth.roomnumber
                  ? null
                  : store.boothRefs.get(booth.roomnumber)}
              button
              key={index}
              onClick={() => {
                if(this.props.onClick) {
                  this.props.onClick(booth)
                }
              }}>
              <ListItemText primary={booth.subject.name} secondary={booth.roomnumber}/>
            </ListItem>
          }
            
          )}
        </List>
      )
    }
}

export default inject('store')(
  observer(
    decorate(MainScreenList, {
      open: observable,
      handleClick: action 
    })
  )
)
