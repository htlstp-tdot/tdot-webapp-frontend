import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import Modal from '@material-ui/core/Modal'
import axios from 'axios'
import IcomoonReact from 'icomoon-react'
import { action, decorate, observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import QRCode from 'qrcode.react'
import React from 'react'
import { buildStyles, CircularProgressbarWithChildren } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import { Redirect } from 'react-router-dom'
import iconSet from '../../../assets/selection.json'
import InfoTextModal from '../../../components/InfoTextModal/InfoTextModal'
import './MainScreen.scss'
import RoomPlan from './RoomPlan'
import MainScreenList from './MainScreenList'
import MainScreenNavbar from './MainScreenNavbar'
import RadialSeparators from './RadialSeparators'
import Sponsors from './Sponsors.js'
import pointJSON from '../../../assets/pointFeatures.json'
import ReactMarkdown from 'react-markdown/with-html'
import { Dialog, DialogContent, DialogTitle } from '@material-ui/core'

class MainScreen extends React.Component {
  qrOpen = false
  modalOpen = false
  selectedBooth
  pointEventContent = ''
  pointEventTitle = 'Punkte'
  lastPoints = -1
  pointEventOpen = false
  pointFeatures = new Map()

  setQrOpen(val) {
    this.qrOpen = val
  }

  checkPointsUpdated() {
    this.pointFeatures.forEach((value,key) => {
      if (key <= this.props.store.visitorStore.points && key > this.lastPoints && Number(sessionStorage.getItem('pointFeature')) < key) {
        this.pointEventContent = this.pointFeatures.get(key)
        this.closeModal()
        this.setQrOpen(false)
        this.openPointEventModal()
      }
    })
    this.lastPoints = this.props.store.visitorStore.points
    sessionStorage.setItem('pointFeature', this.lastPoints)
  }

  componentDidMount() {
    sessionStorage.setItem('pointFeature', -1)
    Object.keys(pointJSON).map((key, index) => this.pointFeatures.set(Number(key), pointJSON[key]))
    this.intervalId = setInterval(this.checkPointsUpdated.bind(this), 1000)
  }
  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  async openModal(booth) {
    if (booth !== undefined && booth.subject !== undefined) {
      this.modalTitle = booth.subject.name
      const res = await axios({
        method: 'get',
        url: `${process.env.REACT_APP_API_URL}/booth/${booth.id}`,
        validateStatus: () => true,
      })
      this.modalContent = res.data !== undefined ? res.data.subject.infotext : ''
      this.modalOpen = true
    }
  }

  closeModal = () => {
    this.modalOpen = false
  }

  openPointEventModal = () => {
    this.pointEventOpen = true
  }

  closePointEventModal = () => {
    this.pointEventOpen = false
  }

  render() {
    let { visitorStore: store } = this.props.store
    if (!store.isLoggedIn) return <Redirect to="/register" />

    return (
      <div>
        <div className="relative max-h-screen">
          <Modal open={this.qrOpen} closeAfterTransition BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
            onClose={() => {
              this.setQrOpen(false)
            }}>
            <Fade in={this.qrOpen}>
              <div className="MainScreen-qr-modal outline-none w-4/5 max-w-sm p-6 bg-white rounded flex justify-center items-center">
                <QRCode value={store.email} renderAs="svg" level="H" className="w-full h-full" />
              </div>
            </Fade>
          </Modal>
        </div>
        <Dialog
          open={this.pointEventOpen}
          onClose={this.closePointEventModal}
        >
          <DialogTitle>
            {this.pointEventTitle}
          </DialogTitle>
          <DialogContent>
            <ReactMarkdown source={this.pointEventContent} escapeHtml={false} />
          </DialogContent>
        </Dialog>
        <InfoTextModal open={this.modalOpen} title={this.modalTitle} content={this.modalContent} onClose={this.closeModal} />
        <MainScreenNavbar />
        <div className="flex justify-between items-center mx-auto max-w-6xl p-3">
          <div className="w-1/3">
            <div className="flex flex-col justify-center items-center h-full">
              <div className="flex justify-center items-center h-16">
                <div className="h-12 w-12">
                  <CircularProgressbarWithChildren
                    className="h-12 w-auto"
                    value={store.visitedBoothCount}
                    maxValue={store.totalBoothCount}
                    strokeWidth={25}
                    styles={buildStyles({
                      strokeLinecap: 'butt',
                      pathColor: '#9cce2b',
                    })}
                  >
                    <RadialSeparators
                      count={store.totalBoothCount}
                      style={{
                        background: '#fff',
                        width: '2px',
                        // This needs to be equal to props.strokeWidth
                        height: `${25}%`,
                      }}
                    />
                  </CircularProgressbarWithChildren>
                </div>
              </div>
              <span className="text-xl font-bold my-bold-font h-10">
                Stationen
              </span>
            </div>
          </div>
          <div className="w-1/3">
            <div className="flex flex-col justify-center items-center h-full">
              <span className="text-5xl font-extrabold my-bold-font h-16">
                {store.points}
              </span>
              <span className="text-xl font-bold my-bold-font h-10">
                Punkte
              </span>
            </div>
          </div>
          <div className="w-1/3 h-32 flex justify-center items-center">
            <div onClick={() => {
              this.setQrOpen(true)
            }}>
              <IcomoonReact iconSet={iconSet} color="#212121" size={72} icon="qrcode" />
            </div>
          </div>
        </div>
        <div className="roomplan-wrapper flex justify-center">
          <RoomPlan />
        </div>
        <MainScreenList onClick={async booth => {
          await this.openModal(booth)
        }} />
        <Sponsors />
      </div>
    )
  }
}

export default inject('store')(
  observer(
    decorate(MainScreen, {
      qrOpen: observable,
      modalOpen: observable,
      modalTitle: observable,
      modalContent: observable,
      pointEventContent: observable,
      pointEventTitle: observable,
      pointEventOpen: observable,
      setQrOpen: action,
      openModal: action,
      closeModal: action,
      openPointEventModal: action,
      closePointEventModal: action,
      checkPointsUpdated: action,
      pointFeatures: observable,
      lastPoints: observable
    })
  )
)
