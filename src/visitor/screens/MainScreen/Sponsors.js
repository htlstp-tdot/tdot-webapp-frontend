import React from "react"
import ListSubheader from "@material-ui/core/ListSubheader"
import sponsors from "../../../assets/sponsors.json"

class Sponsors extends React.Component {
  open = false

  handleClick = () => {
    this.open = !this.open
  }

  render() {
    return (
      <div>
        <div>
          <ListSubheader disableSticky>Sponsoren</ListSubheader>
          <div className="flex flex-col lg:flex-row lg:justify-center flex-wrap">
            {Object.keys(sponsors).map((key, index) => (
              <a
                key={`sponsor-${key}`}
                href={key}
                target="_blank"
                rel="noopener noreferrer"
              >
                <div className="flex justify-center items-center h-24 m-8 max-w-lg">
                  <img
                    className="max-w-full max-h-full"
                    src={`./sponsors/${sponsors[key]}`}
                    alt=""
                  />
                </div>
              </a>
            ))}
          </div>
        </div>
        <div>
          <ListSubheader disableSticky>
            &copy; 2019 Alexander Zach, Manuel Hölzl, Michael König, Florian
            Schlichting, Peter Pressl, Dominik Hofmann
          </ListSubheader>
        </div>
      </div>
    )
  }
}

export default Sponsors
