import React from 'react'
import { SvgLoader, SvgProxy } from 'react-svgmt'
import { action, decorate } from 'mobx'
import { inject, observer } from 'mobx-react'
import './RoomPlan.scss'

class RoomPlan extends React.Component {
  clickAction = e => {
    let { visitorStore: store } = this.props.store
    if (store.boothRefs.has(e.target.parentNode.id)) {
      store.boothRefs.get(e.target.parentNode.id).current.scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      })
    }
  };

  render() {
    let boothMap = {}
    // eslint-disable-next-line no-unused-vars
    for (let booth of this.props.store.visitorStore.booths) {
      if (boothMap[booth.roomnumber] === undefined) {
        boothMap[booth.roomnumber] = { total: 1, visited: 0 }
      } else {
        boothMap[booth.roomnumber].total++
      }
    }
    // eslint-disable-next-line no-unused-vars
    for (let booth of this.props.store.visitorStore.visitedBooths) {
      if (boothMap[booth.roomnumber] !== undefined && boothMap[booth.roomnumber].visited !== undefined)
        boothMap[booth.roomnumber].visited++
    }

    return (
      <SvgLoader className="RoomPlan w-full h-full" path={'raumplan.svg'}>
        {Object.keys(boothMap).map(roomnumber => {
          let opacity =
            boothMap[roomnumber].visited / boothMap[roomnumber].total
          return (
            <SvgProxy
              key={'action' + roomnumber}
              selector={`#${roomnumber} polygon`}
              style={`fill: #9cce2b; opacity: ${opacity}`}
              onClick={this.clickAction}
            />
          )
        })}
      </SvgLoader>
    )
  }
}

export default inject('store')(
  observer(
    decorate(RoomPlan, {
      clickAction: action
    })
  )
)
