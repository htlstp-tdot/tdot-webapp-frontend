import AppBar from '@material-ui/core/AppBar'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import Toolbar from '@material-ui/core/Toolbar'
import IcomoonReact from 'icomoon-react'
import { action, decorate, observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import React from 'react'
import 'react-circular-progressbar/dist/styles.css'
import { withRouter } from 'react-router-dom'
import iconSet from '../../../assets/selection.json'
import './MainScreen.scss'


class MainScreenNavbar extends React.Component {
  open = false

  toggleDrawer = open => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }

    this.open = open
  }

  

  sideList = () => {
    let { visitorStore: store } = this.props.store
    let drawerItemList = [ { text: 'HTL St. Pölten Website',url:'https://htlstp.ac.at' }, { text: 'Anmeldung Schnuppertage', url:'https://www.htlstp.ac.at/formulare/schnuppertag-anmeldung-schuljahr-2019-2020' }, { text: 'Voranmeldung', url: 'https://anmeldung.htlstp.ac.at/anmeldung/' } ]
    return <div
      role="presentation"
      onClick={this.toggleDrawer(false)}
      onKeyDown={this.toggleDrawer(false)}
    >
      <List>
        {drawerItemList.map((obj, idx) => {
          return <ListItem key={obj.text} button onClick={() => {
            if(obj.url)
              window.open(
                obj.url,
                '_blank' 
              )
          }}>
            <ListItemText primary={obj.text} />
          </ListItem>
        })}
        <Divider />
        <ListItem
          button
          onClick={() => {
            store.logout()
            this.props.history.push('/register')
          }}
        >
          <ListItemText primary={'Logout'} />
        </ListItem>
      </List>
    </div>
  }

  render() {
    return (
      <div>
        <SwipeableDrawer
          open={this.open}
          onClose={this.toggleDrawer(false)}
          onOpen={this.toggleDrawer(true)}
        >
          {this.sideList()}
        </SwipeableDrawer>
        <AppBar position="static">
          <Toolbar variant="dense">
            <div className="flex justify-between items-center w-full">
              <div  onClick={this.toggleDrawer(true)}>
                <IcomoonReact iconSet={iconSet} size={24} color="#ffffff" icon="menu"/>
              </div>
              <img
                className="MainScreen-logo h-8"
                src="logo-white.svg"
                alt="HTL St. Pölten"
                style={{ paddingRight: '12px' }}
              />
            </div>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default withRouter(inject('store')(
  observer(
    decorate(MainScreenNavbar, {
      open: observable,
      toggleDrawer: action
    })
  )))
