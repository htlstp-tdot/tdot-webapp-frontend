import React from 'react'
import './LoginWrapper.scss'

class LoginWrapper extends React.Component {

  onFormSubmit = e => {
    if(e.keyCode === 13){
      e.preventDefault()
      if (this.props.onSubmit) this.props.onSubmit()
    }
  }


  render() {
    return (
      <form onKeyDown={this.onFormSubmit}>
        <div className="LoginWrapper-bg">
          <div className="LoginWrapper-overlay flex justify-center items-center">
            <div className="LoginWrapper-container px-16 py-16 rounded-sm shadow-lg">
              {this.props.children}
            </div>
          </div>
        </div>
      </form>
    )
  }
}

export default LoginWrapper
