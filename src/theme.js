import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#9cce2b',
      contrastText: '#212121'
    },
    secondary: {
      main: '#9cce2b',
      contrastText: '#212121'
    },
  },
  shadows: Array(25).fill('none')
})

export default theme
