import { MuiThemeProvider } from '@material-ui/core/styles'
import { observer } from 'mobx-react'
import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import './App.scss'
import LoginScreen from './visitor/screens/LoginScreen/LoginScreen'
import MainScreen from './visitor/screens/MainScreen/MainScreen'
import AdminLoginScreen from './admin/screens/LoginScreen/AdminLoginScreen'
import RegisterScreen from './visitor/screens/RegisterScreen/RegisterScreen'
import ReactAdmin from './admin/screens/ReactAdmin/ReactAdmin'
import theme from './theme'
import ScanScreen from './admin/screens/ScanScreen/ScanScreen'
import ReactGA from 'react-ga'

// <!-- Global site tag (gtag.js) - Google Analytics -->
//     <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151646882-1"></script>
//     <script>
//       window.dataLayer = window.dataLayer || [];
//       function gtag(){dataLayer.push(arguments);}
//       gtag('js', new Date());

//       gtag('config', 'UA-151646882-1');
//       gtag('set', {'user_id': 'USER_ID'}); // Legen Sie die User ID mithilfe des Parameters "user_id" des angemeldeten Nutzers fest.
//       ga('set', 'userId', 'USER_ID'); // Legen Sie die User ID mithilfe des Parameters "user_id" des angemeldeten Nutzers fest.


// store initialisieren
class App extends React.Component {
  render() {
    ReactGA.initialize('UA-151646882-1')
    ReactGA.pageview(window.location.pathname + window.location.search)
    
    return (
      <MuiThemeProvider theme={theme}>
        <Switch>
          <Route exact path="/" component={MainScreen}></Route>
          <Route exact path="/register" component={RegisterScreen}></Route>
          <Route exact path="/login" component={LoginScreen}></Route>
          <Route exact path="/loginAdmin" component={AdminLoginScreen}></Route>
          <Route exact path="/admin" component={ReactAdmin}></Route>
          <Route exact path="/scan" component={ScanScreen}></Route>
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </MuiThemeProvider>
    )
  }
}

export default observer(App)
