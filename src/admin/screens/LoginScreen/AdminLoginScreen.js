import { Button, Checkbox, FormControlLabel, TextField, FormHelperText } from '@material-ui/core/'
import { action, decorate, observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import React from 'react'
import { Redirect } from 'react-router-dom'
import './AdminLoginScreen.scss'
import LoginWrapper from '../../../components/LoginWrapper/LoginWrapper'

class AdminLoginScreen extends React.Component {
  username = ''
  password = ''
  keepSignedIn = false
  errorMessage
  buttonTriggered = false
  store
  power = null

  setErrorMessage = val => {
    this.errorMessage = val
  }

  setButtonTriggered = val => {
    this.buttonTriggered = val
  }

  constructor(props) {
    super(props)
    this.setUsername = this.setUsername.bind(this)
    this.setPassword = this.setPassword.bind(this)
    this.setKeepSignedIn = this.setKeepSignedIn.bind(this)
    this.submit = this.submit.bind(this)
    this.store = this.props.store.adminStore
  }

  setUsername(val) {
    this.username = val
  }

  setPassword(val) {
    this.password = val
  }

  setKeepSignedIn(val) {
    this.keepSignedIn = val
  }

  submit = async () => {
    if (this.username && this.password) {
      let res = await this.store.login(this.username, this.password, this.keepSignedIn)
      if (res === true) {
        //this.props.history.push('/admin')
      } else {
        this.setErrorMessage(res)
      }
    }
    this.setButtonTriggered(true)
  }

  render() {
    if (this.store.getSessionKey() != null) {
      if (this.power === null) {
        action(async () => {
          this.power = await this.store.getPower()
        })()
      }
      if (this.power >= 1000) {
        return <Redirect to="/admin" />
      } else if (this.power !== null) {
        return <Redirect to="/scan" />
      }
    }
    action(() => this.power = null)() 
    return (
      <LoginWrapper onSubmit={this.submit}>
        <img
          src="logo.svg"
          alt="HTL St. Pölten"
          className="block mx-auto w-4/5"
        />
        <div className="mt-8">
          <TextField
            label="Username"
            type="text"
            fullWidth
            margin="none"
            value={this.username}
            onChange={e => this.setUsername(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <TextField
            label="Password"
            type="password"
            autoComplete="password"
            fullWidth
            margin="none"
            value={this.password}
            onChange={e => this.setPassword(e.target.value)}
          />
        </div>
        <FormControlLabel
          control={
            <Checkbox
              style={{ padding: '0.3rem 0.5rem', margin: 'auto' }}
              checked={this.keepSignedIn === true}
              onChange={e => this.setKeepSignedIn(e.target.checked)}
            />
          }
          label="Angemeldet bleiben"
        />
        {this.buttonTriggered && this.username === '' && <FormHelperText error>Benutzername ist erfolderlich</FormHelperText>}
        {this.buttonTriggered && this.password === '' && <FormHelperText error>Passwort ist erfolderlich</FormHelperText>}
        {this.errorMessage && <FormHelperText error>{this.errorMessage}</FormHelperText>}
        <div className="flex justify-center">
          <Button
            variant="contained"
            color="primary"
            margin="none"
            style={{ margin: '2rem 0 0 0' }}
            onClick={this.submit}
          >
            Login
          </Button>
        </div>
      </LoginWrapper>
    )
  }
}

export default inject('store')(
  observer(
    decorate(AdminLoginScreen, {
      username: observable,
      password: observable,
      buttonTriggered: observable,
      errorMessage: observable,
      keepSignedIn: observable,
      power: observable,
      setButtonTriggered: action,
      setErrorMessage: action,
      setUsername: action,
      setPassword: action,
      setKeepSignedIn: action,
      submit: action
    })
  )
)
