import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router-dom'
import { decorate, observable, action } from 'mobx'
import QrReader from 'react-qr-reader'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import axios from 'axios'
import './ScanScreen.scss'
import LogoutButton from '../ReactAdmin/LogoutButton'
import ReactGA from 'react-ga'


class ScanScreen extends React.Component {
  redirectTo = null
  availiableBooths = []
  selectedBoothId = null
  open = false
  snackBarMsg = ''

  constructor(...props) {
    super(...props)
    this.loadBooths()
  }

  handleClick = () => {
    this.setOpen(true)
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    this.setOpen(false)
  }

  setOpen = val => {
    this.open = val
  }

  loadAllBooths = async () => {
    const res = await axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/booths`,
      validateStatus: () => true,
    })
    return res.data.constructor === Array ? res.data : []
  }

  loadBooths = async () => {
    const pwr = await this.props.store.adminStore.getPower()
    if (pwr >= 1000) {
      this.availiableBooths = await this.loadAllBooths()
    } else {
      const res = await axios({
        method: 'get',
        headers: {
          'x-api-key': this.props.store.adminStore.getSessionKey()
        },
        url: `${process.env.REACT_APP_API_URL}/user`,
        validateStatus: () => true,
      })
      if (res.status === 200) {
        this.availiableBooths = res.data.scanPermissions.constructor === Array ? res.data.scanPermissions : []
      } else {
        this.availiableBooths = []
      }
    }
    this.availiableBooths = this.availiableBooths.filter(booth => booth.pointReward > 0)
  }

  unauthorizeMethod() {
    if (!this.props.store.adminStore.getSessionKey()) {
      localStorage.removeItem('session_token')
      this.redirect('/loginAdmin')
    }
    fetch(process.env.REACT_APP_API_URL + '/user', {
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    }).then(response => {
      if (response.status === 401) {
        localStorage.removeItem('session_token')
        this.redirect('/loginAdmin')
      }
    })
  }
  componentDidMount() {
    this.intervalId = setInterval(this.unauthorizeMethod.bind(this), 5000)
  }

  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  redirect(to) {
    this.redirectTo = to
  }

  handleScan = async data => {
    if (data) {
      if (!this.selectedBoothId) {
        this.showMessage('Bitte wähle zuerst eine Station aus!')
        return
      }
      const boothId = this.selectedBoothId
      const res = await axios({
        method: 'post',
        url: `${process.env.REACT_APP_API_URL}/visitor/${data}/visit/`,
        data: {
          boothId
        },
        headers: {
          'x-api-key': this.props.store.adminStore.getSessionKey()
        },
        validateStatus: () => true,
      })
      if (res.status === 204) {
        if(window.navigator && window.navigator.vibrate){
          window.navigator.vibrate(400)
        }
        const allBooths = await this.loadAllBooths()
        const scannedBoothRoomNumber = allBooths.filter(booth => booth.id === boothId)[0].subject.name
        ReactGA.event({
          category: 'Scanning',
          action: 'Booth' + scannedBoothRoomNumber + 'scanned',
          label: data
        })
        this.showMessage('Station wurde als besucht markiert!', true)
       
      } else if (res.status === 400) {
        if(window.navigator && window.navigator.vibrate){
          window.navigator.vibrate([ 100,100 ])
        }
        this.showMessage('Diese Station wurde bereits besucht!', true)
      }
      
    }
  }

  handleError = err => {
    console.error(err)
  }

  setSelectedBoothId = e => {
    this.selectedBoothId = e.target.value
  }

  showMessage = (msg, onlyIfClosed = false) => {
    if ((onlyIfClosed && this.open === false) || !onlyIfClosed) {
      this.snackBarMsg = msg
      this.setOpen(true)
    }
  }

  render() {
    let sessionKey = this.props.store.adminStore.getSessionKey()
    if (!sessionKey) return <Redirect to="/loginAdmin" />
    if (this.redirectTo) return <Redirect to={this.redirectTo} />

    return <div className="min-h-screen flex flex-col items-center">
      <div className="ScanScreen-qr w-full flex justify-center items-center mb-16">
        <QrReader
          delay={300}
          onError={this.handleError}
          onScan={this.handleScan}
          showViewFinder={false}
          style={{ width: '100%', maxWidth: '60vh' }}
        />
      </div>
      <div>
        <InputLabel htmlFor="selectedBooth">Station</InputLabel>
        <Select
          value={this.selectedBoothId ? this.selectedBoothId : 0}
          inputProps={{
            id: 'selectedBooth',
          }}
          onChange={this.setSelectedBoothId}
          style={{ minWidth: '10rem', maxWidth: '100vw' }}
        >
          {this.availiableBooths.map((booth, index) =>
            <MenuItem key={booth.id} value={booth.id}>{booth.subject.name}</MenuItem>
          )}
        </Select>
      </div>
      <div>
        <LogoutButton onClick={() => this.redirect('/loginAdmin')}/>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={this.open}
        autoHideDuration={2000}
        onClose={() => { this.setOpen(false) }}
      >
        <SnackbarContent
          message={this.snackBarMsg}
        />
      </Snackbar>
    </div>
  }
}

export default inject('store')(observer(decorate(ScanScreen, {
  redirectTo: observable,
  redirect: action,
  availiableBooths: observable,
  open: observable,
  setOpen: action,
  selectedBoothId: observable,
  handleScan: action,
  setSelectedBoothId: action,
  snackBarMsg: observable,
  showMessage: action,
  loadAllBooths: observable,
  loadBooths: observable
})))
