import React from 'react'
import { SimpleForm, TextInput, BooleanInput, Edit, required, email, NumberInput } from 'react-admin'

const VisitorEditTitle = ({ record }) => (
  <span>
    Besucher {record ? record.email : ''} ändern
  </span>
)

export const VisitorEdit = props => (
  <Edit title={ <VisitorEditTitle/> } {...props}>
    <SimpleForm>
      <TextInput disabled label="Email Adresse" source="email" type="email" validate={[required('Email erforderlich'), email('Muss eine valide Email sein')]} />
      <BooleanInput source="preregistration_isAllowed" label="Voranmeldung erlaubt" />
      <NumberInput source="score" label="Punkte" validate={ required('Punkte erforderlich') } defaultValue={0}/>
      <BooleanInput source="alreadyWon" label="Gewonnen" />
    </SimpleForm>
  </Edit>
)

export default VisitorEdit
