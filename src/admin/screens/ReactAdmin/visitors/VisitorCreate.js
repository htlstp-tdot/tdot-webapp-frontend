import React from 'react'
import { required,  Create, SimpleForm, TextInput, BooleanInput, NumberInput, email } from 'react-admin'

export const VisitorCreate = props => (
  <Create title="Besucher hinzufügen" {...props}>
    <SimpleForm>
      <TextInput label="Email Adresse" source="email" type="email" validate={[required('Email erforderlich'), email('Muss eine valide Email sein')]} />
      <BooleanInput source="preregistration_isAllowed" label="Voranmeldung erlaubt" />
      <NumberInput source="score" label="Punkte" defaultValue={0}/>
      <BooleanInput source="alreadyWon" label="Gewonnen" />
    </SimpleForm>
  </Create>
)

export default VisitorCreate
  
