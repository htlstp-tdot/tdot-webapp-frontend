import React from 'react'
import {  Datagrid, List, TextField, NumberField, BooleanField } from 'react-admin'

const VisitorList = props => (
  <List {...props}>
    <Datagrid>
      <NumberField source="idNum" label="ID" />
      <TextField source="email" label="Email" />
      <BooleanField source="preregistration_isAllowed" label="Voranmeldung erlaubt" />
      <NumberField source="score" label="Punkte" />
      <BooleanField source="alreadyWon" label="Gewonnen" />
    </Datagrid>
  </List>
)

export default VisitorList
