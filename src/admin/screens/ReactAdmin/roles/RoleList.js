import React from 'react'
import {  Datagrid, List, TextField, NumberField } from 'react-admin'

export const RoleList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <NumberField source="id" label="ID" />
      <TextField source="name" label="Name" />
      <NumberField source="power" label="Befugnis" />
    </Datagrid>
  </List>
)

export default RoleList
