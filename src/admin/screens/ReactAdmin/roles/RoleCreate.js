import React from 'react'
import { required, number, Create, SimpleForm, TextInput, NumberInput } from 'react-admin'

export const RoleCreate = props => (
  <Create title="Rolle hinzufügen" {...props}>
    <SimpleForm>
      <TextInput source="rolename" label="Name" validate={required('Rollenname erfoderlich')}  />
      <NumberInput source="power" label="Befugnis" validate={number('Befugnis erfoderlich')} />
    </SimpleForm>
  </Create>
)

export default RoleCreate
  
