import React from 'react'
import { SimpleForm, TextInput, Edit, required, number, NumberInput } from 'react-admin'

const RoleEditTitle = ({ record }) => (
  <span>
    Rolle {record ? record.name : ''} ändern
  </span>
)

export const RoleEdit = props => (
  <Edit title={ <RoleEditTitle/> } {...props}>
    <SimpleForm>
      <TextInput source="name" label="Name" validate={required('Rollenname erfoderlich')}  />
      <NumberInput source="power" label="Befugnis" validate={number('Befugnis erfoderlich')} />
    </SimpleForm>
  </Edit>
)

export default RoleEdit
