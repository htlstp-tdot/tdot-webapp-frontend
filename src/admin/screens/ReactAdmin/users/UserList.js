import React from 'react'
import { Datagrid, List, TextField, ReferenceField } from 'react-admin'

export const UserList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="username" />
      <ReferenceField label="Rolle" source="role.id" reference="roles">
        <TextField source="name" />
      </ReferenceField>
    </Datagrid>
  </List>
)

export default UserList
