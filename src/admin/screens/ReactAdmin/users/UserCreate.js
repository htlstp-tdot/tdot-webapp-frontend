import React from 'react'
import { required,  Create, SimpleForm, TextInput, ReferenceInput, SelectInput } from 'react-admin'

export const UserCreate = props => (
  <Create title="User hinzufügen" {...props}>
    <SimpleForm>
      <TextInput source="username" validate={required('Username ist erforderlich')} />
      <TextInput label="Passwort" source="password" validate={required('Passwort ist erforderlich')}/>
      <ReferenceInput label="Rolle" source="id" reference="roles">
        <SelectInput optionText="name" validate={required('Rolle ist erforderlich')} optionValue="id" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
)

export default UserCreate
  
