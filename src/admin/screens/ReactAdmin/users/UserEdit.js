import React from 'react'
import { SimpleForm, TextInput, ReferenceInput, SelectInput, Edit, required } from 'react-admin'

const UserEditTitle = ({ record }) => (
  <span>
      User {record ? record.username : ''} ändern
  </span>
) 
  
export const UserEdit = props => (
  <Edit title={ <UserEditTitle/> } {...props}>
    <SimpleForm>
      <TextInput disabled source="username" />
      <TextInput label="Passwort" source="password" validate={required('Passwort ist erforderlich')}/>
      <ReferenceInput label="Rolle" source="role.power" reference="roles">
        <SelectInput optionText="name" validate={required('Rolle ist erforderlich')} optionValue="power" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
)

export default UserEdit
