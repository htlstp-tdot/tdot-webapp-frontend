import SubjectCreate from './SubjectCreate'
import SubjectEdit from './SubjectEdit'
import SubjectList from './SubjectList'

export default {
  list: SubjectList,
  create: SubjectCreate,
  edit: SubjectEdit
}
