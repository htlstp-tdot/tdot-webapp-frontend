import React from 'react'
import { SimpleForm, TextInput, Edit, required, LongTextInput } from 'react-admin'

const SubjectEditTitle = ({ record }) => (
  <span>
    Schulfach {record ? record.name : ''} ändern
  </span>
)


export const SubjectEdit = props => (
  <Edit title={ <SubjectEditTitle/> } {...props}>
    <SimpleForm>
      <TextInput source="name" label="Schulfach" validate={required('Schulfachname erfoderlich')} />
      <LongTextInput source="infotext" label="Infotext" />
    </SimpleForm>
  </Edit>
)

export default SubjectEdit
