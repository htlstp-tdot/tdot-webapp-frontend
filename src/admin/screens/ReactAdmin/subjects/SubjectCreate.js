import React from 'react'
import { required,  Create, SimpleForm, TextInput, LongTextInput } from 'react-admin'

export const SubjectCreate = props => (
  <Create title="Schulfach hinzufügen" {...props}>
    <SimpleForm>
      <TextInput source="name" label="Schulfach" validate={required('Schulfachname erfoderlich')} />
      <LongTextInput source="infotext" label="Infotext" default={''} />
    </SimpleForm>
  </Create>
)

export default SubjectCreate
  
