import React from 'react'
import {  Datagrid, List, TextField, NumberField } from 'react-admin'

const SubjectList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <NumberField source="id" label="ID" />
      <TextField source="name" label="Name" />
      <NumberField source="infotext" label="Infotext" />
    </Datagrid>
  </List>
)

export default SubjectList
