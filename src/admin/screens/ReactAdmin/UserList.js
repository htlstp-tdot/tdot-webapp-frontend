import React from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  Switch,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button
} from '@material-ui/core';
import { inject, observer } from 'mobx-react';
import { decorate, observable, action } from 'mobx';
import './ReactAdmin.scss';
import iconSet from '../../../assets/selection.json';
import IcomoonReact from 'icomoon-react';
import { Redirect } from 'react-router-dom';

class UserList extends React.Component {
  users = [];
  openModal = false;
  currentUser = {
    id: 0,
    username: '',
    role: {
      id: 0,
      name: 0
    }
  };
  booths = [];
  fullScreen = window.innerWidth < 720;

  constructor(props) {
    super(props)
    this.getUsers = this.getUsers.bind(this)
  }

  unauthorizeMethod() {
    if (!this.props.store.adminStore.getSessionKey()) {
      localStorage.removeItem('session_token')
      this.setState({})
      this.redirect('/loginAdmin')
    }
    fetch(process.env.REACT_APP_API_URL + '/user', {
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    }).then(response => {
      if (response.status === 401) {
        localStorage.removeItem('session_token')
        this.setState({})
        this.redirect('/loginAdmin')
      }
    })
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      this.fullScreen = window.innerWidth < 720
    })
    this.getUsers()
    this.intervalId = setInterval(this.getUsers, 10000)
    this.intervalId2 = setInterval(this.unauthorizeMethod.bind(this), 1000)
  }
  componentWillUnmount() {
    clearInterval(this.intervalId)
    clearInterval(this.intervalId2)
  }

  handleSwitch(event, userid, booth, index) {
    fetch(
      process.env.REACT_APP_API_URL +
        '/user/' +
        userid +
        '/scanPermissions/' +
        booth.id,
      {
        method: event.target.checked ? 'POST' : 'DELETE',
        headers: {
          'x-api-key': this.props.store.adminStore.getSessionKey()
        }
      }
    ).then(response => {
      if (response.status === 204 || response.status === 200)
        this.booths[index].isGranted = !this.booths[index].isGranted
    })
  }

  async callUserModal(user) {
    this.currentUser = user
    this.booths = []
    const scanPermissions = await fetch(
      process.env.REACT_APP_API_URL + '/users',
      {
        method: 'GET',
        headers: {
          'x-api-key': this.props.store.adminStore.getSessionKey()
        }
      }
    )
      .then(response => {
        return response.json()
      })
      .then(response => {
        response.forEach(responseUser => {
          if (responseUser.id === user.id)
            response.scanPermissions = responseUser.scanPermissions
        })
        return response.scanPermissions
      })
    scanPermissions.forEach(booth => {
      this.booths.push({
        id: booth.id,
        name: booth.subject.name,
        isGranted: true
      })
    })
    const allBooths = await fetch(process.env.REACT_APP_API_URL + '/booths', {
      method: 'GET',
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    })
      .then(response => {
        return response.json()
      })
      .then(response => {
        return response
      })
    allBooths.forEach(booth => {
      let found = false
      this.booths.forEach(b => {
        if (b.id === booth.id) {
          found = true
        }
      })
      if (!found) {
        this.booths.push({
          id: booth.id,
          name: booth.subject.name,
          isGranted: false
        })
      }
    })
    this.setModalOpen(true)
  }

  setModalOpen(val) {
    this.openModal = val
  }

  async getUsers() {
    let res = await fetch(process.env.REACT_APP_API_URL + '/users', {
      method: 'GET',
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    })
      .then(response => {
        return response.json()
      })
      .then(response => {
        return response
      })
    this.users = res.constructor === Array ? res : []
  }

  render() {
    let sessionKey = this.props.store.adminStore.getSessionKey()
    if (!sessionKey) return <Redirect to="/loginAdmin" />

    return (
      <div className="relative max-h-screen">
        <Dialog
          fullScreen={this.fullScreen}
          open={this.openModal}
          onClose={() => {
            this.setModalOpen(false)
          }}
          className="p-12"
        >
          <DialogTitle>
            Scanberechtigungen von {this.currentUser.username}
          </DialogTitle>
          <DialogContent>
            <List className="overflow-auto">
              {this.booths.map((booth, index) => {
                return (
                  <ListItem key={`${booth.id}`}>
                    <ListItemText primary={`${booth.name}`} />
                    <ListItemSecondaryAction>
                      <Switch
                        edge="end"
                        onChange={event =>
                          this.handleSwitch(
                            event,
                            this.currentUser.id,
                            booth,
                            index
                          )
                        }
                        checked={this.booths[index].isGranted}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                )
              })}
            </List>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={() => this.setModalOpen(false)}>
              Close
            </Button>
          </DialogActions>
        </Dialog>

        <List dense>
          <ul>
            {this.users.map(item => (
              <ListItem
                dense={item.role.power < 1000}
                button={item.role.power < 1000}
                key={`item-${item.id}`}
                onClick={() =>
                  item.role.power < 1000 ? this.callUserModal(item) : null
                }
              >
                <ListItemIcon>
                  {item.role.power >= 1000 ? (
                    <IcomoonReact
                      iconSet={iconSet}
                      color="#212121"
                      size={30}
                      icon="user-tie"
                    />
                  ) : (
                    <IcomoonReact
                      iconSet={iconSet}
                      color="#212121"
                      size={30}
                      icon="qrcode"
                    />
                  )}
                </ListItemIcon>
                <ListItemText primary={`${item.username}`} />
              </ListItem>
            ))}
          </ul>
        </List>
      </div>
    )
  }
}

export default inject('store')(
  observer(
    decorate(UserList, {
      setModalOpen: action,
      getUsers: action,
      users: observable,
      openModal: observable,
      currentUser: observable,
      booths: observable,
      unauthorizeMethod: action,
      fullScreen: observable
    })
  )
)
