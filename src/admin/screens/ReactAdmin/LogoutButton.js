
import React from 'react'
import { Button } from '@material-ui/core/'
import { inject, observer } from 'mobx-react'
import IcomoonReact from 'icomoon-react'
import iconSet from '../../../assets/selection.json'

class LogoutButton extends React.Component {
  
  render() {
    return <div className="flex justify-center">
      <Button
        variant="contained"
        color="primary"
        margin="none"
        style={{ margin: '2rem 0 0 0' }}
        onClick={() => {
          this.props.store.adminStore.logout()
          if (this.props.onClick) {
            this.props.onClick()
          }
        }}
      >
        <IcomoonReact iconSet={iconSet} size={24} color="#ffffff" icon="exit" />
        Logout
      </Button>
    </div>
  }
}

export default inject('store')(
  observer(
    LogoutButton
  )
)
