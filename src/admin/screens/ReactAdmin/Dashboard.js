import React from 'react'
import { Button, CardActions, CardContent, Card, Modal, Fade, Backdrop, Switch, FormControl, FormControlLabel, Typography } from '@material-ui/core'
import { inject, observer } from 'mobx-react'
import { decorate, observable, action } from 'mobx'
import './ReactAdmin.scss'
import UserList from './UserList'
import { Redirect } from 'react-router-dom'

class Dashboard extends React.Component {
  nosave = false
  visitorCount = 0
  openModal = false
  content = {
    'email': 'email@example.com',
    'score': 0
  }

  constructor(props) {
    super(props)
    this.calcWinner.bind(this)
  }

  getVisitorCount() {
    fetch(process.env.REACT_APP_API_URL + '/visitors?offset=0&limit=0&sortDirection=ASC', {
      method: 'GET',
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    }).then(response => response.json())
      .then(response => {
        this.visitorCount = response.length
      })
  }

  unauthorizeMethod() {
    if(!this.props.store.adminStore.getSessionKey()){
      localStorage.removeItem('session_token')
      this.setState({})
      this.redirect('/loginAdmin')
    }
    fetch( process.env.REACT_APP_API_URL + '/user', {
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    }).then(response => {
      if(response.status === 401){
        localStorage.removeItem('session_token')
        this.setState({})
        this.redirect('/loginAdmin')
      }
    })
  }

  componentDidMount() {
    this.intervalId = setInterval(this.getVisitorCount.bind(this), 10000)
    this.intervalId2 = setInterval(this.unauthorizeMethod.bind(this),1000)
  }
  componentWillUnmount() {
    clearInterval(this.intervalId)
    clearInterval(this.intervalId2)
  }

  setModalOpen(state) {
    this.openModal = state
  }

  calcWinner() {
    fetch(process.env.REACT_APP_API_URL + '/visitors/winner?nosave=' + (this.nosave ? 'true' : 'false'), {
      method: 'GET',
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    }).then(response => {
      return response.json()
    }).then(response => {
      if(response.code === 401){
        
      }
      if(response.code === 400){
        this.content = {}
      }else {
        this.content= response
      }
      this.setModalOpen(true)
    })
  }

  render() {
    //TODO: List with winners
    let sessionKey = this.props.store.adminStore.getSessionKey()
    if (!sessionKey) return <Redirect to="/loginAdmin" />

    let modalContent
    if(Object.keys(this.content).length < 2) {
      modalContent =  <div><h2>Fehler</h2><br/><p>Keine gueltigen Besucher gefunden</p></div>
    }else{
      modalContent = <div><h2>Neuer Gewinner</h2><br/><p>Email: {this.content.email}</p><br/><p>Score: {this.content.score}</p></div>
    }
    return <div className="relative max-h-screen">
      <Modal open={this.openModal} closeAfterTransition BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        onClose={() => {
          this.setModalOpen(false)
        }}>
        <Fade in={this.openModal}>
          <div className="AdminScreen-modal outline-none w-4/5 max-w-sm p-6 bg-white rounded flex justify-center items-center">
            {modalContent}
          </div>
        </Fade>
      </Modal>

      <Card className='m-2 full-width'>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h3">
            Visitors Overview
          </Typography>
          <Typography variant="body" color="textPrimary" component="p">
            Visitors: {this.visitorCount}
          </Typography>
          <FormControl component="fieldset">
            <FormControlLabel
              value="start"
              control={
                <Switch
                  checked={!this.nosave}
                  onChange={() => {this.nosave = !this.nosave}}
                  color="primary"
                />
              }
              label="Ergebnis speichern"
              labelPlacement="start"
            />
          </FormControl>
        </CardContent>
        <CardActions>
          <Button float="right" size="small" color="primary" onClick={this.calcWinner.bind(this)}>
            Gewinner auslosen
          </Button>
        </CardActions>
      </Card>
      <Card className='m-2'>
        <CardContent>
          <a href='scan'>Click here for Scanner View</a>
        </CardContent>
      </Card>
      <div className="flex justify-center">
        <div className="inline-block m-2 w-full">
          <Card>
            <CardContent>
              List with winners
            </CardContent>
          </Card>
        </div>
        <div className="inline-block m-2 w-full">
          <Card>
            <CardContent>
              List with users - Scannermanagement
              <UserList/>
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  }
}

export default inject('store')(observer(decorate(Dashboard, {
  nosave: observable,
  visitorCount: observable,
  openModal: observable,
  content: observable,
  setModalOpen: action,
  calcWinner: action,
  getVisitorCount: action,
  renderModalText: action,
  unauthorizeMethod: action
})))
