import React from 'react'
import { required,  Create, SimpleForm, TextInput, ReferenceInput, SelectInput, NumberInput, number } from 'react-admin'

export const BoothCreate = props => (
  <Create title="Stand hinzufügen" {...props}>
    <SimpleForm>
      <NumberInput source="pointReward" label="Punkte" default={0} validate={number('Punkte erforderlich')} />
      <TextInput source="roomnumber" label="Raumnummer" validate={required('Raumnummer erforderlich')} />
      <ReferenceInput label="Schulfach" source="subjectId" reference="subjects">
        <SelectInput source="name" validate={required('Schulfach ist erforderlich')} />
      </ReferenceInput>
    </SimpleForm>
  </Create>
)

export default BoothCreate
  
