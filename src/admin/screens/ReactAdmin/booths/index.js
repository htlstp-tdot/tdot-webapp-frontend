import BoothCreate from './BoothCreate'
import BoothEdit from './BoothEdit'
import BoothList from './BoothList'

export default {
  list: BoothList,
  create: BoothCreate,
  edit: BoothEdit
}
