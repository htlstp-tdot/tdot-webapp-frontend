import React from 'react'
import { SimpleForm, NumberInput, TextInput, ReferenceInput, SelectInput, Edit, required, number } from 'react-admin'

const BoothEditTitle = ({ record }) => (
  <span>
    Stand {record ? record.subject.name : ''} ändern
  </span>
)

export const BoothEdit = props => (
  <Edit title={ <BoothEditTitle/> } {...props}>
    <SimpleForm>
      <NumberInput source="pointReward" label="Punkte" default={0} validate={number('Punkte erforderlich')} />
      <TextInput source="roomnumber" label="Raumnummer" validate={required('Raumnummer erforderlich')} />
      <ReferenceInput label="Schulfach" source="subject.id" reference="subjects">
        <SelectInput source="name" validate={required('Schulfach ist erforderlich')} />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
)

export default BoothEdit
