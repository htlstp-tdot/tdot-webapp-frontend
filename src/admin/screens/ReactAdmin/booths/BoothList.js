import React from 'react'
import {  Datagrid, List, NumberField, ReferenceField, TextField } from 'react-admin'


const BoothList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <NumberField source="id" label="ID" />
      <NumberField source="pointReward" label="Punkte" />
      <TextField source="roomnumber" label="Raumnummer"  />
      <ReferenceField label="Schulfach" source="subject.id" reference="subjects">
        <TextField source="name" />
      </ReferenceField>
    </Datagrid>
  </List>
)


export default BoothList
