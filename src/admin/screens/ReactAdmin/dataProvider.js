import { stringify } from 'query-string'
import {
  fetchUtils,
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  UPDATE_MANY,
  DELETE,
  DELETE_MANY,
} from 'react-admin'

/**
 * Maps react-admin queries to a simple REST API
 *
 * The REST dialect is similar to the one of FakeRest
 * @see https://github.com/marmelab/FakeRest
 * @example
 * GET_LIST     => GET http://my.api.url/posts?sort=['title','ASC']&range=[0, 24]
 * GET_ONE      => GET http://my.api.url/posts/123
 * GET_MANY     => GET http://my.api.url/posts?filter={ids:[123,456,789]}
 * UPDATE       => PUT http://my.api.url/posts/123
 * CREATE       => POST http://my.api.url/posts
 * DELETE       => DELETE http://my.api.url/posts/123
 */
export default (apiUrl, sessionKey, httpClient = fetchUtils.fetchJson) => {
  /**
  * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The data request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
  const convertDataRequestToHTTP = (type, resource, params) => {
    let url = ''
    const options = {}
    switch (type) {
    case GET_LIST: {
      const { page, perPage } = params.pagination
      const { field, order } = params.sort
      const query = {
        sort: field,
        sortDirection: order,
        offset: (page - 1) * perPage,
        limit: page * perPage - 1,
      }
      url = `${apiUrl}/${resource}?${stringify(query)}`
      break
    }
    case GET_ONE:
      url = `${apiUrl}/${resource.slice(0, resource.length - 1)}/${params.id}`
      break
    case GET_MANY: {
      const query = {
        filter: JSON.stringify({ id: params.ids }),
      }
      url = `${apiUrl}/${resource}?${stringify(query)}`
      break
    }
    case GET_MANY_REFERENCE: {
      const { page, perPage } = params.pagination
      const { field, order } = params.sort
      const query = {
        sort: field,
        sortDirection: order,
        offset: (page - 1) * perPage,
        limit: page * perPage - 1,
      }
      url = `${apiUrl}/${resource}?${stringify(query)}`
      break
    }
    case UPDATE:
      url = `${apiUrl}/${resource.slice(0, resource.length - 1)}/${params.id}`
      options.method = 'PUT'
      if (resource === 'users') {
        let body = { id: params.data.id, username: params.data.username, power: params.data.role.power }
        if(params.data.password) body.password = params.data.password
        options.body = JSON.stringify(body)
        break
      }
      if (resource === 'booths') {
        options.body = JSON.stringify({ pointReward: params.data.pointReward, subjectId: params.data.subject.id, roomNumber: params.data.roomNumber })
      }

      options.body = JSON.stringify(params.data)
      break
    case CREATE:
      url = `${apiUrl}/${resource.slice(0, resource.length - 1)}`
      options.method = 'POST'
      if (resource === 'users') {
        options.body = JSON.stringify({ username: params.data.username, password: params.data.password, roleId: params.data.id })
        break
      }
      options.body = JSON.stringify(params.data)
      break
    case DELETE:
      url = `${apiUrl}/${resource.slice(0, resource.length - 1)}/${params.id}`
      options.method = 'DELETE'
      break
    default:
      throw new Error(`Unsupported fetch action type ${type}`)
    }
    return { url, options }
  }

  /**
     * @param {Object} response HTTP response from fetch()
     * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
     * @param {String} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params The data request params, depending on the type
     * @returns {Object} Data response
     */
  const convertHTTPResponse = (response, type, resource, params) => {
    const { headers, json } = response
    switch (type) {
    case GET_LIST:
    case GET_MANY_REFERENCE:
      if (!headers.has('content-range')) {
        throw new Error(
          'The Content-Range header is missing in the HTTP Response. The simple REST data provider expects responses for lists of resources to contain this header with the total number of results to build the pagination. If you are using CORS, did you declare Content-Range in the Access-Control-Expose-Headers header?'
        )
      }
      if (resource === 'visitors') {
        for (let i = 0; i < json.length; i++) {
          json[i].idNum = json[i].id
          json[i].id = json[i].email
        }
      }
      return {
        data: json,
        total: parseInt(
          headers
            .get('content-range')
            .split('/')
            .pop(),
          10
        ),
      }
    case CREATE:
      return { data: { ...params.data, id: 0 } }
    case DELETE_MANY: {
      return { data: json || [] }
    }
    default:
      return { data: json || { id: 0 } }
    }
  }

  /**
     * @param {string} type Request type, e.g GET_LIST
     * @param {string} resource Resource name, e.g. "posts"
     * @param {Object} payload Request parameters. Depends on the request type
     * @returns {Promise} the Promise for a data response
     */
  return (type, resource, params) => {
    // simple-rest doesn't handle filters on UPDATE route, so we fallback to calling UPDATE n times instead
    if (type === UPDATE_MANY) {
      return Promise.all(
        params.ids.map(id =>
          httpClient(`${apiUrl}/${resource.slice(0, resource.length - 1)}/${id}`, {
            method: 'PUT',
            headers: JSON.stringify({ 'x-api-key': sessionKey }),
            body: JSON.stringify(params.data),
          })
        )
      ).then(responses => ({
        data: responses.map(response => response.json),
      }))
    }
    // simple-rest doesn't handle filters on DELETE route, so we fallback to calling DELETE n times instead
    if (type === DELETE_MANY) {
      return Promise.all(
        params.ids.map(id =>
          httpClient(`${apiUrl}/${resource.slice(0, resource.length - 1)}/${id}`, {
            headers: new Headers({ 'x-api-key': sessionKey }),
            method: 'DELETE',
          })
        )
      ).then(responses => ({
        data: responses.map(response => response.json),
      }))
    }

    const { url, options } = convertDataRequestToHTTP(
      type,
      resource,
      params
    )
    if (!options.headers) {
      options.headers = new Headers({ 'x-api-key': sessionKey })
    }
    return httpClient(url, options).then(response =>
      convertHTTPResponse(response, type, resource, params)
    )
  }
}

