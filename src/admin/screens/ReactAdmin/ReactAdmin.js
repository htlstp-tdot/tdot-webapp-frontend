import React from 'react'
import { Admin, Resource } from 'react-admin'
import { inject, observer } from 'mobx-react'
import AdminLoginScreen from '../LoginScreen/AdminLoginScreen'
import dataProvider from './dataProvider'
import { Redirect } from 'react-router-dom'
import users from './users'
import visitors from './visitors'
import roles from './roles'
import booths from './booths'
import subjects from './subjects'
import theme from '../../../theme'
import LogoutButton from './LogoutButton'
import { decorate, observable, action } from 'mobx'
import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'ra-core'
import Dashboard from './Dashboard'

const authProvider = (type, params) => {
  switch(type) {
  case AUTH_LOGIN:
  case AUTH_LOGOUT:
  case AUTH_ERROR:
  case AUTH_CHECK:
  default:
    break
  }
  return Promise.resolve()
}

class ReactAdmin extends React.Component {
  redirectTo = null

  unauthorizeMethod() {
    if(!this.props.store.adminStore.getSessionKey()){
      localStorage.removeItem('session_token')
      this.setState({})
    }
    fetch( process.env.REACT_APP_API_URL + '/user', {
      headers: {
        'x-api-key': this.props.store.adminStore.getSessionKey()
      }
    }).then(response => {
      if(response.status === 401){
        localStorage.removeItem('session_token')
        this.setState({})
      }
    })
  }
  
  componentDidMount() {
    this.intervalId = setInterval(this.unauthorizeMethod.bind(this), 1000)
  }
  componentWillUnmount(){
    clearInterval(this.intervalId)
  }

  redirect(to) {
    this.redirectTo = to
  }

  render() {
    let sessionKey = this.props.store.adminStore.getSessionKey()
    if (!sessionKey) return <Redirect to="/loginAdmin" />
    this.props.store.adminStore.getPower().then(pwr => {
      if (pwr < 1000) {
        this.props.history.push('/scan')
      }
    })
    return <Admin dashboard={Dashboard} theme={theme} loginScreen={AdminLoginScreen} authProvider={authProvider} logoutButton={LogoutButton} dataProvider={dataProvider(process.env.REACT_APP_API_URL, sessionKey)}>
      <Resource name="visitors" {...visitors} />
      <Resource name="users" {...users} />
      <Resource name="roles" {...roles} />
      <Resource name="booths" {...booths} />
      <Resource name="subjects" {...subjects} />
    </Admin>
  }
}

export default inject('store')(observer(decorate(ReactAdmin, {
  redirectTo: observable,
  redirect: action
})))
