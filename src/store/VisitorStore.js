import axios from 'axios'
import React from 'react'
import { action, computed, decorate, observable } from 'mobx'

class VisitorStore {
  isLoggedIn = false;
  email;
  points = 0;
  visitedBooths = [];
  booths = [];
  keepUpdatedInterval;
  boothRefs = new Map();

  constructor() {
    let email = localStorage.getItem('email')
    if (email !== null) {
      this.email = email
      this.successfulLogin()
    }
  }

  async successfulLogin() {
    this.isLoggedIn = true
    this.fetchBooths()
    this.fetchVisitedBooths()
    this.keepUpdatedInterval = setInterval(this.fetchVisitedBooths, 3000)
  }

  fetchBooths = async () => {
    const res = await axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/booths`,
      validateStatus: () => true
    })
    this.booths = res.data.constructor === Array ? res.data : []
    this.booths.sort((a, b) => String(a.roomnumber).localeCompare(b.roomnumber))
      .forEach(element => {
        this.boothRefs.set(element.roomnumber, React.createRef())
      })
  };

  fetchVisitedBooths = async () => {
    const res = await axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/visitor/${this.email}`,
      validateStatus: () => true
    })
    this.points = typeof res.data.score === 'number' ? res.data.score : 0
    if (res.data.visitedBooths) this.visitedBooths = res.data.visitedBooths
  };

  register = async (email, preregistration) => {
    const res = await axios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/visitor`,
      validateStatus: () => true,
      data: {
        email: email,
        preregistration_isAllowed: preregistration
      }
    })
    if (res.status === 204) {
      this.email = email
      await this.successfulLogin()
      localStorage.setItem('email', email)
      return true
    } else {
      return 'Benutzer exisiert bereits. Bitte loggen Sie sich ein.'
    }
  };

  login = async email => {
    const res = await axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/visitor/${email}`,
      validateStatus: () => true
    })
    if (res.status === 200) {
      this.email = email
      await this.successfulLogin()
      localStorage.setItem('email', email)
      return true
    } else {
      return 'Benutzer wurde nicht gefunden. Bitte registrieren Sie sich zuerst.'
    }
  };

  logout = () => {
    this.isLoggedIn = false
    localStorage.removeItem('email')
    clearInterval(this.keepUpdatedInterval)
  };

  /**
   * Get totalBoothCount where each booth has to have points
   */
  get totalBoothCount() {
    return this.booths !== undefined ? this.booths.filter(booth => booth.pointReward > 0).length : 0
  }

  /**
   * Get visitedBoothCount where each booth has to have points
   */
  get visitedBoothCount() {
    return this.visitedBooths !== undefined ? this.visitedBooths.filter(booth => booth.pointReward > 0).length : 0
  }
}

export default decorate(VisitorStore, {
  boothRefs: observable,
  isLoggedIn: observable,
  email: observable,
  points: observable,
  visitedBooths: observable,
  visitedBoothCount: computed,
  booths: observable,
  totalBoothCount: computed,
  register: action,
  logout: action
})
