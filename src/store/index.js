import AdminStore from './AdminStore'
import VisitorStore from './VisitorStore'

export default class RootStore {
    visitorStore = new VisitorStore()
    adminStore = new AdminStore()
}
