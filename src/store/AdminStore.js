import { decorate, observable, action } from 'mobx'

class AdminStore {
  session_token = null

  constructor() {
    let token = localStorage.getItem('session_token')
    if (token === null) {
      token = sessionStorage.getItem('session_token')
    }
    this.session_token = token
  }

  async getPower(){
    return await fetch(`${process.env.REACT_APP_API_URL}/user`, {
      headers: {
        'x-api-key': this.getSessionKey()
      }
    }).then(response => {
      return response.json()
    }).then(response => {
      if(response && response.role) return response.role.power
      return false
    })
  }

  getSessionKey = () => {
    return this.session_token
  }

  setSessionKey = (val, stayLoggedIn = false) => {
    this.session_token = val
    if (stayLoggedIn) localStorage.setItem('session_token', val)
    else sessionStorage.setItem('session_token', val)
  }

  logout = () => {
    this.session_token = null
    localStorage.removeItem('session_token')
    sessionStorage.removeItem('session_token')
  }

  logoutEverywhere = () => {
    fetch(`${process.env.REACT_APP_API_URL}/logout`, {
      method: 'POST',
      headers: {
        'x-api-key': this.getSessionKey()
      }
    })
    this.logout()
  }

  async login(username, password, stayLoggedIn = false) {
    let body = {
      'username': username,
      'password': password
    }
    let result = await fetch(`${process.env.REACT_APP_API_URL}/login`, {
      method: 'POST',
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }).then(response => {
      if (response.status === 200) {
        return response.json()
      } else if(response.status === 401 || response.status === 400){
        return 'Benutzerdaten sind ungültig'
      } else {
        return 'Fehler'
      }
    }).then(response => {
      if(response.key){
        this.setSessionKey(response.key, stayLoggedIn)
        return true
      }else{
        return response
      }
    })
    return result
  }

}

export default decorate(AdminStore, {
  session_token: observable,
  setSessionKey: action,
  getSessionKey: action,
  logout: action,
  logoutEverywhere: action,
  login: action
})
